﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Маги1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Маги1.Tests
{
    [TestClass]
    public class PointTests
    {
        private double _eps = 0.0000001;

        [TestMethod]
        public void PointIsCreatedFromCoordinates()
        {
            var point = new Point(2, 1);

            Assert.AreEqual(point.X, 2, _eps);
            Assert.AreEqual(point.Y, 1, _eps);
        }

        [TestMethod]
        public void PointXCoordinateIsChangeable()
        {
            var point = new Point(2, 1);

            point.X = 3;

            Assert.AreEqual(point.X, 3, _eps);
            Assert.AreEqual(point.Y, 1, _eps);
        }

        [TestMethod]
        public void PointYCoordinateIsChangeable()
        {
            var point = new Point(2, 1);

            point.Y = 3;

            Assert.AreEqual(point.X, 2, _eps);
            Assert.AreEqual(point.Y, 3, _eps);
        }

        [TestMethod]
        public void PointDistanceFromCenterIsCorrect()
        {
            var point = new Point(-3, -4);

            Assert.AreEqual(point.Ro, 5, _eps);
        }

        [TestMethod]
        public void PointDistanceFromCenterChangeIsCorrect()
        {
            var point = new Point(-3, -4);

            point.Ro = 10;

            Assert.AreEqual(point.Ro, 10, _eps);
            Assert.AreEqual(point.X, -6, _eps);
            Assert.AreEqual(point.Y, -8, _eps);
        }
    }
}