﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Маги1;

namespace Маги1.Tests
{
    [TestClass]
    public class LineTests
    {
        private double _eps = 0.0000001;

        [TestMethod]
        public void DefaultConstructorIsCorrect()
        {
            var line = new Line("Линия");

            var firstPoint = new Point(0, 0);
            var secondPoint = new Point(1, 1);

            Assert.AreEqual(firstPoint, line.A);
            Assert.AreEqual(secondPoint, line.B);
        }

        [TestMethod]
        public void PointConstructorIsCorrect()
        {
            var firstPoint = new Point(2, 3);
            var secondPoint = new Point(4, 5);

            var line = new Line("Линия", firstPoint, secondPoint);

            Assert.AreEqual(firstPoint, line.A);
            Assert.AreEqual(secondPoint, line.B);
        }

        [TestMethod]
        public void OriginalAPointChangesLinePoint()
        {
            var point = new Point(-1, 0);
            var line = new Line("Линия", point, new Point(1, 0));

            point.X = -2;

            Assert.AreEqual(-2, line.A.X);
        }

        [TestMethod]
        public void OriginalBPointChangesLinePoint()
        {
            var point = new Point(1, 0);
            var line = new Line("Линия", new Point(-1, 0), point);

            point.X = -2;

            Assert.AreEqual(-2, line.B.X);
        }

        [TestMethod]
        public void APointIsChangeable()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));

            line.A.X = -2;

            Assert.AreEqual(-2, line.A.X);
        }

        [TestMethod]
        public void BPointIsChangeable()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));

            line.B.X = -2;

            Assert.AreEqual(-2, line.B.X);
        }

        [TestMethod]
        public void OnChangingEventIsTriggeredOnACoordinatesChanging()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));
            var handlerCalled = false;

            line.OnChanging += (l, e) => {
                handlerCalled = true;
            };

            line.A.X = -2;

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnChangingEventIsTriggeredOnBCoordinatesChanging()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));
            var handlerCalled = false;

            line.OnChanging += (l, e) => {
                handlerCalled = true;
            };

            line.B.X = -2;

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnChangeEventIsTriggeredOnACoordinatesChange()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));
            var handlerCalled = false;

            line.OnChange += (l) => {
                handlerCalled = true;
            };

            line.A.X = -2;

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnChangeEventIsTriggeredOnBCoordinatesChange()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));
            var handlerCalled = false;

            line.OnChange += (l) => {
                handlerCalled = true;
            };

            line.B.X = -2;

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnChangingEventIsCancelableOnAPropertyChanging()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));

            line.OnChanging += (l, e) => {
                e.Cancel = true;
            };

            line.A.X = -2;

            Assert.AreEqual(-1, line.A.X);
        }

        [TestMethod]
        public void OnChangingEventIsCancelableOnBPropertyChanging()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));

            line.OnChanging += (l, e) => {
                e.Cancel = true;
            };

            line.B.X = -2;

            Assert.AreEqual(-1, line.A.X);
        }

        [TestMethod]
        public void OnChangingEventPassesNewCoordinatesOnACoordinatesChanging()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));
            double? newX = null;

            line.OnChanging += (l, e) => {
                newX = e.A.X;
            };

            line.A.X = -2;

            Assert.AreEqual(-2, newX);
        }

        [TestMethod]
        public void OnChangingEventPassesNewCoordinatesOnBCoordinatesChanging()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));
            double? newX = null;

            line.OnChanging += (l, e) => {
                newX = e.B.X;
            };

            line.B.X = -2;

            Assert.AreEqual(-2, newX);
        }

        [TestMethod]
        public void OnChangingEventPassesNewCoordinatesOnAPropertyChanging()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));
            Point newA = null;

            line.OnChanging += (l, e) => {
                newA = e.A;
            };

            line.A = new Point(-2, -1);

            Assert.AreEqual(line.A, newA);
        }

        [TestMethod]
        public void OnChangingEventPassesNewCoordinatesOnBPropertyChanging()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));
            Point newB = null;

            line.OnChanging += (l, e) => {
                newB = e.B;
            };

            line.B = new Point(-2, -1);

            Assert.AreEqual(line.B, newB);
        }

        [TestMethod]
        public void OnChangingEventIsTriggeredOnAPropertyChanging()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnChanging += (l, e) => {
                handlerCalled = true;
            };

            line.A = new Point(-1, -1);

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnChangingEventIsTriggeredOnBPropertyChanging()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnChanging += (l, e) => {
                handlerCalled = true;
            };

            line.B = new Point(-1, -1);

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnChangeEventIsTriggeredOnAPropertyChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnChange += (l) => {
                handlerCalled = true;
            };

            line.A = new Point(-1, -1);

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnChangeEventIsTriggeredOnBPropertyChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnChange += (l) => {
                handlerCalled = true;
            };

            line.B = new Point(-1, -1);

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void EventsAreTriggeredInRightOrderOnAPropertyChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var order = 1;

            line.OnChanging += (l, e) => {
                if (order++ != 1)
                {
                    Assert.Fail();
                }
            };
            line.OnChange += (l) => {
                if (order++ != 2)
                {
                    Assert.Fail();
                }
            };
            line.OnCross += (l, e) => {
                if (order++ != 3)
                {
                    Assert.Fail();
                }
            };

            line.A = new Point(-1, -1);
        }

        [TestMethod]
        public void EventsAreTriggeredInRightOrderOnBPropertyChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var order = 1;

            line.OnChanging += (l, e) => {
                if (order++ != 1)
                {
                    Assert.Fail();
                }
            };
            line.OnChange += (l) => {
                if (order++ != 2)
                {
                    Assert.Fail();
                }
            };
            line.OnCross += (l, e) => {
                if (order++ != 3)
                {
                    Assert.Fail();
                }
            };

            line.B = new Point(-1, -1);
        }

        [TestMethod]
        public void OnCrossEventIsTriggeredOnAPropertyQuarterChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnCross += (l, e) => {
                handlerCalled = true;
            };

            line.A = new Point(-1, -1);

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnCrossEventIsTriggeredOnBPropertyQuarterChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnCross += (l, e) => {
                handlerCalled = true;
            };

            line.B = new Point(-1, -1);

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnCrossEventIsTriggeredOnACoordinatesQuarterChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnCross += (l, e) => {
                handlerCalled = true;
            };

            line.A.X = -1;

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnCrossEventIsTriggeredOnBCoordinatesQuarterChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnCross += (l, e) => {
                handlerCalled = true;
            };

            line.B.X = -1;

            Assert.IsTrue(handlerCalled);
        }

        [TestMethod]
        public void OnCrossEventIsNotTriggeredOnACoordinatesInSameQuarterChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnCross += (l, e) => {
                handlerCalled = true;
            };

            line.A.X = 2;

            Assert.IsFalse(handlerCalled);
        }

        [TestMethod]
        public void OnCrossEventIsNotTriggeredOnBCoordinatesInSameQuarterChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnCross += (l, e) => {
                handlerCalled = true;
            };

            line.B.X = 2;

            Assert.IsFalse(handlerCalled);
        }

        [TestMethod]
        public void OnCrossEventIsNotTriggeredOnAPropertyInSameQuarterChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnCross += (l, e) => {
                handlerCalled = true;
            };

            line.A = new Point(2, 1);

            Assert.IsFalse(handlerCalled);
        }

        [TestMethod]
        public void OnCrossEventIsNotTriggeredOnBPropertyInSameQuarterChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnCross += (l, e) => {
                handlerCalled = true;
            };

            line.B = new Point(2, 1);

            Assert.IsFalse(handlerCalled);
        }

        [TestMethod]
        public void OnCrossEventIsNotTriggeredOnBothPointsChange()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var handlerCalled = false;

            line.OnCross += (l, e) => {
                handlerCalled = true;
            };

            line.SetLine(new Point(-2, -2), new Point(-1, -1));

            Assert.IsFalse(handlerCalled);
        }

        [TestMethod]
        public void OnCrossEventPassesOldCoordinatesOnSetLine()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            Point oldA = null;
            Point oldB = null;

            line.OnCross += (l, e) => {
                oldA = e.A;
                oldB = e.B;
            };

            line.SetLine(new Point(-2, -2), new Point(1, 1));

            Assert.AreEqual(new Point(1, 1), oldA);
            Assert.AreEqual(new Point(2, 2), oldB);
        }

        [TestMethod]
        public void OnChangingEventIsCancelableOnSetLineChanging()
        {
            var line = new Line("Линия", new Point(-1, 0), new Point(1, 0));

            line.OnChanging += (l, e) => {
                e.Cancel = true;
            };

            line.SetLine(new Point(-2, -1), new Point(3, 4));

            Assert.AreEqual(new Point(-1, 0), line.A);
            Assert.AreEqual(new Point(1, 0), line.B);
        }

        [TestMethod]
        public void OnChangingEventCalledOnceOnSetLine()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var callTimes = 0;

            line.OnChanging += (l, e) => {
                callTimes += 1;
            };

            line.SetLine(new Point(-2, -2), new Point(-1, -1));

            Assert.AreEqual(1, callTimes);
        }

        [TestMethod]
        public void OnChangeEventCalledOnceOnSetLine()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var callTimes = 0;

            line.OnChange += (l) => {
                callTimes += 1;
            };

            line.SetLine(new Point(-2, -2), new Point(-1, -1));

            Assert.AreEqual(1, callTimes);
        }


        [TestMethod]
        public void OnCrossEventCalledOnceOnSetLine()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var callTimes = 0;

            line.OnCross += (l, e) => {
                callTimes += 1;
            };

            line.SetLine(new Point(-2, -2), new Point(1, 1));

            Assert.AreEqual(1, callTimes);
        }

        [TestMethod]
        public void DistanceToRightHandPointIsCorrect()
        {
            var line = new Line("Линия", new Point(0, 0), new Point(1, 0));
            var point = new Point(2, 1);

            Assert.AreEqual(Math.Sqrt(2), line.RoFrom(point), _eps);
        }

        [TestMethod]
        public void DistanceToLeftHandPointIsCorrect()
        {
            var line = new Line("Линия", new Point(0, 0), new Point(1, 0));
            var point = new Point(-1, 1);

            Assert.AreEqual(Math.Sqrt(2), line.RoFrom(point), _eps);
        }

        [TestMethod]
        public void DistanceToPerpendicularPointIsCorrect()
        {
            var line = new Line("Линия", new Point(0, 0), new Point(1, 0));
            var point = new Point(0.5, 1);

            Assert.AreEqual(1, line.RoFrom(point), _eps);
        }

        [TestMethod]
        public void DistanceToLineIsCorrect()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var otherLine = new Line("Линия2", new Point(3, 0), new Point(2, 1));

            Assert.AreEqual(0.7071067812, line.RoFrom(otherLine), _eps);
        }

        [TestMethod]
        public void DistanceToIntersectingLineIsZero()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            var otherLine = new Line("Линия2", new Point(2, 1), new Point(1, 2));

            Assert.AreEqual(0, line.RoFrom(otherLine), _eps);
        }

        [TestMethod]
        public void LengthIsCorrect()
        {
            var line = new Line("Линия", new Point(3, 0), new Point(0, 4));

            Assert.AreEqual(5, line.Len, _eps);
        }

        [TestMethod]
        public void AngleOfVerticalLineIsHalfPi()
        {
            var line = new Line("Линия", new Point(0, 0), new Point(0, 1));

            var radians = line.RadiansOverXAxis();

            Assert.AreEqual(Math.PI / 2, radians, _eps);
        }

        [TestMethod]
        public void AngleOfHorizontalLineIsZero()
        {
            var line = new Line("Линия", new Point(0, 0), new Point(1, 0));

            var radians = line.RadiansOverXAxis();

            Assert.AreEqual(0, radians, _eps);
        }

        [TestMethod]
        public void AngleOfAscendingLineIsCorrect()
        {
            var line = new Line("Линия", new Point(-2, -1), new Point(1, 2));

            var radians = line.RadiansOverXAxis();

            Assert.AreEqual(Math.PI / 4, radians, _eps);
        }

        [TestMethod]
        public void AngleOfDescendingLineIsCorrect()
        {
            var line = new Line("Линия", new Point(-4, 3), new Point(-2, 1));

            var radians = line.RadiansOverXAxis();

            Assert.AreEqual(-Math.PI / 4, radians, _eps);
        }

        [TestMethod]
        public void StretchVerticalLineIsCorrect()
        {
            var line = new Line("Линия", new Point(0, 0), new Point(0, 1));

            line.Len = line.Len * 3;

            Assert.AreEqual(new Point(0, -1), line.A);
            Assert.AreEqual(new Point(0, 2), line.B);
        }

        [TestMethod]
        public void StretchHorizontalLineIsCorrect()
        {
            var line = new Line("Линия", new Point(5, 2), new Point(3, 2));

            line.Len = line.Len * 2;

            Assert.AreEqual(new Point(6, 2), line.A);
            Assert.AreEqual(new Point(2, 2), line.B);
        }

        [TestMethod]
        public void StretchAscendingLineIsCorrect()
        {
            var line = new Line("Линия", new Point(1, 2), new Point(-2, -1));

            line.Len = line.Len * 1.5;

            Assert.AreEqual(new Point(1.75, 2.75), line.A);
            Assert.AreEqual(new Point(-2.75, -1.75), line.B);
        }

        [TestMethod]
        public void StretchDescendingLineIsCorrect()
        {
            var line = new Line("Линия", new Point(6, -5), new Point(2, -1));

            line.Len = line.Len * 1.5;

            Assert.AreEqual(new Point(7, -6), line.A);
            Assert.AreEqual(new Point(1, 0), line.B);
        }

        [TestMethod]
        public void ShrinkVerticalLineIsCorrect()
        {
            var line = new Line("Линия", new Point(0, -1), new Point(0, 1));

            line.Len = line.Len / 2;

            Assert.AreEqual(new Point(0, -0.5), line.A);
            Assert.AreEqual(new Point(0, 0.5), line.B);
        }

        [TestMethod]
        public void ShrinkHorizontalLineIsCorrect()
        {
            var line = new Line("Линия", new Point(9, 0), new Point(-1, 0));

            line.Len = line.Len / 2;

            Assert.AreEqual(new Point(6.5, 0), line.A);
            Assert.AreEqual(new Point(1.5, 0), line.B);
        }

        [TestMethod]
        public void ShrinkAscendingLineIsCorrect()
        {
            var line = new Line("Линия", new Point(1, 2), new Point(-2, -1));

            line.Len = line.Len * 0.5;

            Assert.AreEqual(new Point(0.25, 1.25), line.A);
            Assert.AreEqual(new Point(-1.25, -0.25), line.B);
        }

        [TestMethod]
        public void ShrinkDescendingLineIsCorrect()
        {
            var line = new Line("Линия", new Point(6, -5), new Point(2, -1));

            line.Len = line.Len * 0.5;

            Assert.AreEqual(new Point(5, -4), line.A);
            Assert.AreEqual(new Point(3, -2), line.B);
        }

        [TestMethod]
        public void CenterIsCorrect()
        {
            var line = new Line("Линия", new Point(1, 1), new Point(-1, -1));

            var center = line.C;

            Assert.AreEqual(new Point(0, 0), center);
        }

        [TestMethod]
        public void CenterChangeIsCorrect()
        {
            var line = new Line("Линия", new Point(-4, -3), new Point(-2, -1));

            line.C = new Point(4, 4);

            Assert.AreEqual(new Point(3, 3), line.A);
            Assert.AreEqual(new Point(5, 5), line.B);
            Assert.AreEqual(new Point(4, 4), line.C);
        }
    }
}
