﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Маги1
{
    public class Line
    {
        public event Action<Line> OnChange;
        public event Action<Line, OnLineChangingEventArgs> OnChanging;
        public event Action<Line, OnLineCrossEventArgs> OnCross;
        public string Name;

        private Point _a = new Point(0, 0);
        private Point _b = new Point(0, 0);
        private Point _oldA, _oldB;

        private double _eps = 0.0000001;

        public Line(string name) : this(name, new Point(0, 0), new Point(1, 1)) { }

        public Line(string name, Point a, Point b)
        {
            Name = name;
            SetLine(a, b);
        }

        public Point A
        {
            get
            {
                return _a;
            }
            set
            {
                SetLine(value, B);
            }
        }

        public Point B
        {
            get
            {
                return _b;
            }
            set
            {
                SetLine(A, value);
            }
        }

        public Point C
        {
            get
            {
                return new Point(A.X + (B.X - A.X) / 2.0, A.Y + (B.Y - A.Y) / 2.0);
            }
            set
            {
                var oldCenter = C;
                var newCenter = value;

                var xDiff = newCenter.X - oldCenter.X;
                var yDiff = newCenter.Y - oldCenter.Y;

                var newA = A.Copy();
                newA.MoveRel(xDiff, yDiff);
                var newB = B.Copy();
                newB.MoveRel(xDiff, yDiff);

                SetLine(newA, newB);
            }
        }

        public double Len
        {
            get
            {
                return B.RoFrom(A);
            }
            set
            {
                var originalLength = Len;
                var newLength = value;
                var lengthDiff = newLength - originalLength;

                var angle = RadiansOverXAxis();

                var xAxisHalfDiff = Math.Abs( Math.Cos(angle) ) * lengthDiff / 2;
                var yAxisHalfDiff = Math.Abs( Math.Sin(angle) ) * lengthDiff / 2;

                var c = C;
                var a = A.Copy();
                var b = B.Copy();

                a.X += ( a.X > c.X ? 1 : -1) * xAxisHalfDiff;
                a.Y += ( a.Y > c.Y ? 1 : -1) * yAxisHalfDiff;
                b.X += ( b.X > c.X ? 1 : -1) * xAxisHalfDiff;
                b.Y += ( b.Y > c.Y ? 1 : -1) * yAxisHalfDiff;

                SetLine(a, b);
            }
        }

        public double RadiansOverXAxis()
        {
            var height = Math.Abs(B.Y - A.Y);
            var length = Math.Abs(B.X - A.X);
            
            //Вертикальная линия
            if ( length<_eps )
            {
                return Math.PI / 2;
            }

            var k = (B.Y - A.Y) / (B.X - A.X);
            var asc = Math.Sign( k );
            var radians = asc * Math.Atan(height/length);

            return radians;
        }

        public void MoveRel(double deltaX, double deltaY)
        {
            var a = A.Copy();
            a.MoveRel(deltaX, deltaY);
            var b = B.Copy();
            b.MoveRel(deltaX, deltaY);
            SetLine(a, b);
        }

        public void Assign(Line line)
        {
            SetLine(line.A.Copy(), line.B.Copy());
        }

        public void SimOX()
        {
            var a = A.Copy();
            a.SimOX();
            var b = B.Copy();
            b.SimOX();
            SetLine(a, b);
        }

        public void SimOY()
        {
            var a = A.Copy();
            a.SimOY();
            var b = B.Copy();
            b.SimOY();
            SetLine(a, b);
        }

        public void SimO()
        {
            var a = A.Copy();
            a.SimO();
            var b = B.Copy();
            b.SimO();
            SetLine(a, b);
        }

        public double RoFrom(Point p)
        {
            var v = new Point(B.X-A.X,B.Y-A.Y);
            var w = new Point(p.X-A.X,p.Y-A.Y);

            var c1 = VectorDot(w, v);
            if (c1 <= 0)
            {
                return p.RoFrom(A);
            }

            var c2 = VectorDot(v, v);
            if (c2 <= c1)
            {
                return p.RoFrom(B);
            }

            var b = c1 / c2;
            var Pb = new Point(A.X + b * v.X, A.Y + b * v.Y);
            return p.RoFrom(Pb);
        }

        private static double VectorDot(Point lhs, Point rhs)
        {
            return lhs.X * rhs.X + lhs.Y * rhs.Y;
        }

        public double RoFrom(Line other)
        {
            if ( HasIntersection(other) )
            {
                return 0;
            }
            var distances = new[] { other.RoFrom(A), other.RoFrom(B), RoFrom(other.A), RoFrom(other.B) };
            return distances.Min();
        }

        public bool HasIntersection(Line l)
        {
            if (A.Equals(l.A) || B.Equals(l.B) || A.Equals(l.B) || B.Equals(l.A))
                return true;
            var v1 = VectorM(l.B.X - l.A.X, l.B.Y - l.A.Y, A.X - l.A.X, A.Y - l.A.Y);
            var v2 = VectorM(l.B.X - l.A.X, l.B.Y - l.A.Y, B.X - l.A.X, B.Y - l.A.Y);
            var v3 = VectorM(B.X - A.X, B.Y - A.Y, l.A.X - A.X, l.A.Y - A.Y);
            var v4 = VectorM(B.X - A.X, B.Y - A.Y, l.B.X - A.X, l.B.Y - A.Y);
            if ((v1 * v2) < 0 && (v3 * v4) < 0)
                return true;
            return false;
        }

        private double VectorM(double ax, double ay, double bx, double by)
        {
            return ax * by - bx * ay;
        }

        private void PointAChangingHandler(Point point, OnPointChangingEventArgs args)
        {
            SetOldPoints();

            var aPoint = new Point(args.X, args.Y);
            var onLineChangingEventArgs = new OnLineChangingEventArgs(aPoint, B);

            PointChangingHandler(onLineChangingEventArgs, args);
        }

        private void PointBChangingHandler(Point point, OnPointChangingEventArgs args)
        {
            SetOldPoints();

            var bPoint = new Point(args.X, args.Y);
            var onLineChangingEventArgs = new OnLineChangingEventArgs(A, bPoint);

            PointChangingHandler(onLineChangingEventArgs, args);
        }

        private void PointChangingHandler(OnLineChangingEventArgs lineArgs, OnPointChangingEventArgs pointArgs)
        {
            OnChanging?.Invoke(this, lineArgs);
            if (lineArgs.Cancel)
            {
                pointArgs.Cancel = true;
            }
        }

        private void PointChangedHandler(Point unused)
        {
            OnChange?.Invoke(this);
            
            if (_oldA.Q == _oldB.Q && A.Q != B.Q)
            {
                var onCrossEventArgs = new OnLineCrossEventArgs(_oldA, _oldB);
                OnCross?.Invoke(this, onCrossEventArgs);
            }
        }

        public void SetLine(Point newA, Point newB)
        {
            if (newA==null || newB==null)
            {
                throw new NullReferenceException();
            }

            SetOldPoints();

            var onLineChangingEventArgs = new OnLineChangingEventArgs(newA, newB);
            OnChanging?.Invoke(this, onLineChangingEventArgs);
            if (onLineChangingEventArgs.Cancel)
            {
                return;
            }

            _a.OnChanging -= PointAChangingHandler;
            _a.OnChange -= PointChangedHandler;
            _a = newA;
            _a.OnChanging += PointAChangingHandler;
            _a.OnChange += PointChangedHandler;
            
            _b.OnChanging -= PointBChangingHandler;
            _b.OnChange -= PointChangedHandler;
            _b = newB;
            _b.OnChanging += PointBChangingHandler;
            _b.OnChange += PointChangedHandler;

            PointChangedHandler(null);
        }

        private void SetOldPoints()
        {
            _oldA = _a.Copy();
            _oldB = _b.Copy();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Line))
            {
                return false;
            }
            var other = obj as Line;
            return Name==other.Name && ( ( A.Equals(other.A) && B.Equals(other.B) ) || ( A.Equals(other.B) && B.Equals(other.A) ) );
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ A.GetHashCode() ^ B.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("( {0} : {1} ) ", A, B);
        }
    }
}
