﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Маги1
{
    class Program
    {
        private static double _eps = 0.0000001;

        public static void Main(string[] args)
        {
            DemonstratePoint();
            DemonstrateLine();

            Console.WriteLine("Всё");
            Console.ReadLine();
        }

        public static void DemonstratePoint()
        {
            Console.WriteLine("Точка: ");
            var point = new Point(3, 4);
            point.OnChanging += (p, e) => { Console.WriteLine("OnChanging: " + point + " " + new Point(e.X, e.Y)); };
            point.OnChange += (p) => { Console.WriteLine("OnChange:" + point); };
            point.OnAxis += (p, e) => { Console.WriteLine("OnAxis: " + point + " " + new Point(e.X, e.Y)); };

            Console.WriteLine("X: " + point.X);
            Console.WriteLine("Y: " + point.Y);
            Console.WriteLine("Ro: " + point.Ro);
            Console.WriteLine("Q: " + point.Q);
            Console.WriteLine("Point: " + point);
            Console.WriteLine("RoFrom[7,7]: " + point.RoFrom(new Point(7, 7)));

            Console.ReadLine();

            point.SetPoint(2, 1);
            Console.WriteLine("SetPoint(2,1): " + point);

            point.SimO();
            Console.WriteLine("SimO: " + point);

            point.SimOX();
            Console.WriteLine("SimOX: " + point);

            point.SimOY();
            Console.WriteLine("SimOY: " + point);

            Console.ReadLine();

            point.MoveRel(-4, 2);
            Console.WriteLine("MoveRel(-4,2): " + point);

            Console.WriteLine("Equals[2,2]: " + point.Equals(new Point(2, 2)));

            Console.WriteLine("Equals[X,Y]: " + point.Equals(new Point(point.X, point.Y)));

            point.SetPoint(0, 1);
            Console.WriteLine("SetPoint(0,1): " + point);
        }

        public static void DemonstrateLine()
        {
            Console.WriteLine("Линия: ");
            var line = new Line("Линия", new Point(1, 1), new Point(2, 2));
            line.OnChange += (l) => { Console.WriteLine("Отрезок изменился: " + l.ToString() ); };
            line.OnChanging += (l, e) =>
            {
                var newL = new Line("",e.A, e.B);
                if (newL.Len < _eps || newL.Len > 10)
                {
                    e.Cancel = true;
                    Console.WriteLine("Отрезок слишком длинный: " + newL + ". Запрещено изменение");
                }
            };
            line.OnChanging += (l, e) =>
            {
                if ( e.A.Q==e.B.Q )
                {
                    return;
                }
                Console.WriteLine("Концы отрезка "+l.Name+" в разных четвертях. "+e.A+", четверть "+e.A.Q+"; "+e.B+ ", четверть "+e.B.Q);
                Console.WriteLine("Разрешить? Y/N ");
                e.Cancel = Console.ReadLine().ToLower()!="y";
            };

            Console.WriteLine("Изменяю линию на "+ new Point(1, 1) + " "+ new Point(2, 1));
            line.SetLine(new Point(1, 1), new Point(2, 1));
            Console.WriteLine(line.ToString());

            Console.WriteLine("Изменяю линию на " + new Point(0, 1) + " " + new Point(2, 1));
            line.SetLine(new Point(0, 1), new Point(2, 1));
            Console.WriteLine(line.ToString());

            Console.ReadLine();

            Console.WriteLine("Изменяю линию на " + new Point(1, 1) + " " + new Point(20, 20));
            line.SetLine(new Point(1, 1), new Point(20, 20));
            Console.WriteLine(line.ToString());

            Console.WriteLine("Изменяю линию на " + new Point(-1, -1) + " " + new Point(2, 3));
            line.SetLine(new Point(-1, -1), new Point(2, 3));
            Console.WriteLine(line.ToString());

            Console.ReadLine();

            var firstLine = new Line("Другая линия", new Point(0, 0), new Point(3, 3));
            var secondLine = new Line("Другая линия", new Point(1, 0), new Point(4, 3));
            firstLine.OnChanging += (l, e) =>
            {
                var newL = new Line("", e.A, e.B);
                if ( newL.HasIntersection(secondLine) )
                {
                    e.Cancel = true;
                    Console.WriteLine("Отрезки "+ newL + " и "+secondLine+ " пересекаются. Запрещено изменение "+l);
                }
            };
            firstLine.SetLine(new Point(1, 0), new Point(0, 3));
        }
    }
}
