﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Маги1
{
    public class Point
    {
        public event Action<Point> OnChange;
        public event Action<Point, OnPointChangingEventArgs> OnChanging;
        public event Action<Point, OnPointAxisEventArgs> OnAxis;

        private double _eps = 0.0000001;
        private double _x, _y;

        public Point(double x, double y)
        {
            SetPoint(x, y);
        }

        public Point(Point other)
        {
            SetPoint(other.X, other.Y);
        }

        public double X
        {
            get {
                return _x;
            }
            set {
                SetPoint(value, _y);
            }
        }
        public double Y
        {
            get
            {
                return _y;
            }
            set
            {
                SetPoint(_x, value);
            }
        }

        public int Q
        {
            get
            {
                if (X > _eps && Y > _eps)
                {
                    return 1;
                }

                if (X < -_eps && Y > _eps)
                {
                    return 2;
                }

                if (X < -_eps && Y < -_eps)
                {
                    return 3;
                }

                if (X > _eps && Y < -_eps)
                {
                    return 4;
                }

                return 0;
            }
        }

        public double Ro
        {
            get
            {
                var center = new Point(0, 0);

                return RoFrom(center);
            }
            set
            {
                var oldDistance = Ro;
                var newDistance = value;
                var multiplier = newDistance / oldDistance;

                var newX = multiplier * X;
                var newY = multiplier * Y;

                SetPoint(newX, newY);
            }
        }

        public double RoFrom(Point other)
        {
            return Math.Sqrt((X - other.X) * (X - other.X) + (Y - other.Y) * (Y - other.Y));
        }

        public void SetPoint(double newX, double newY)
        {
            if ( Math.Abs(X-newX)<_eps && Math.Abs(Y-newY)<_eps )
            {
                return;
            }

            var oldX = X;
            var oldY = Y;

            var onPointChangingEventArgs = new OnPointChangingEventArgs(newX, newY);
            OnChanging?.Invoke(this, onPointChangingEventArgs);
            if (onPointChangingEventArgs.Cancel)
            {
                return;
            }

            _x = newX;
            _y = newY;
            OnChange?.Invoke(this);

            if (( Math.Abs(oldX)>_eps && Math.Abs(X)<_eps ) || (Math.Abs(oldY)>_eps && Math.Abs(Y) < _eps))
            {
                var onPointAxisEventArgs = new OnPointAxisEventArgs(oldX, oldY);
                OnAxis?.Invoke(this, onPointAxisEventArgs);
            }
        }

        public void MoveRel(double deltaX, double deltaY)
        {
            SetPoint(X + deltaX, Y + deltaY);
        }

        public void SimOX()
        {
            SetPoint(-X, Y);
        }

        public void SimOY()
        {
            SetPoint(X, -Y);
        }

        public void SimO()
        {
            SetPoint(-X, -Y);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Point))
            {
                return false;
            }
            var other = obj as Point;
            return Math.Abs(X-other.X)<_eps && Math.Abs(Y-other.Y)<_eps;
        }

        public override int GetHashCode()
        {
            return (int)Math.Round(X) ^ (int)Math.Round(Y);
        }

        public override string ToString()
        {
            return string.Format("[{0},{1}]", X, Y);
        }

        public Point Copy()
        {
            return new Point(this);
        }
    }
}
