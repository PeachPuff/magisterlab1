﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Маги1
{
    public class OnLineChangingEventArgs : EventArgs
    {
        public bool Cancel;

        public OnLineChangingEventArgs(Point a, Point b)
        {
            A = new Point(a);
            B = new Point(b);
        }

        public Point A
        {
            get; private set;
        }
        public Point B
        {
            get; private set;
        }
    }
}
