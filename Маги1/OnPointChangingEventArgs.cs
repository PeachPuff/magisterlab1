﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Маги1
{
    public class OnPointChangingEventArgs : EventArgs
    {
        public bool Cancel;

        public OnPointChangingEventArgs(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X
        {
            get; private set;
        }
        public double Y
        {
            get; private set;
        }
    }
}
